"""Duolingo vocabulary extraction

Extracts learned Duolingo (https://www.duolingo.com/) vocabulary with
their English translations using the Unofficial Duolingo API
(https://github.com/KartikTalwar/Duolingo) and produces CSV and PDF files
(via LaTeX (https://www.latex-project.org/)).
"""

# import libraries
from configparser import ConfigParser
from getpass import getpass
from json import dump
from os import remove, rename, system
import pandas as pd
from duolingo import Duolingo

# input login information in command line
myusername = input('Enter your Duolingo username: ')
mypassword = getpass('Enter your Duolingo password: ')

# enter login information
lingo = Duolingo(myusername, password=mypassword)

# import list of languages learned by user
config = ConfigParser()
config.read('languages.conf')

# convert to list
languageList = [
    z.strip() for z in config.get('languages', 'languages').split('\n')
]
# remove empty strings
languageList = list(filter(None, languageList))

# dictionary of languages and their codes
languageCodes = {
    'Arabic': 'ar',
    'Chinese': 'zs',
    'Czech': 'cz',
    'Danish': 'da',
    'Dutch': 'dn',
    'Esperanto': 'eo',
    'Finnish': 'fi',
    'French': 'fr',
    'German': 'de',
    'Greek': 'el',
    'Haitian Creole': 'ht',
    'Hawaiian': 'hw',
    'Hebrew': 'he',
    'High Valyrian': 'hv',
    'Hindi': 'hi',
    'Hungarian': 'hu',
    'Indonesian': 'id',
    'Irish': 'ga',
    'Italian': 'it',
    'Japanese': 'jp',
    'Klingon': 'kl',
    'Korean': 'ko',
    'Latin': 'la',
    'Navajo': 'nv',
    'Norwegian (Bokmål)': 'nb',
    'Polish': 'pl',
    'Portuguese': 'pt',
    'Romanian': 'ro',
    'Russian': 'ru',
    'Scottish Gaelic': 'gd',
    'Spanish': 'es',
    'Swahili': 'sw',
    'Swedish': 'sv',
    'Turkish': 'tr',
    'Ukrainian': 'uk',
    'Vietnamese': 'vi',
    'Yiddish': 'yi',
    'Welsh': 'cy'
}

# get list of languages learned by the user and their codes
languages = []
for language in languageList:
    for n, c in languageCodes.items():
        if n == language:
            languages.append((c, n))

# extracting data for each language in a loop
for code, name in languages:
    # extract vocabulary data from Duolingo's vocabulary overview
    vocab = lingo.get_vocabulary(language_abbr=code)

    # save vocabulary data as json
    # use indent > 0 to pretty print the output
    with open('vocab_' + code + '.json', mode='w', encoding='utf-8') as f:
        dump(vocab, f, indent=2)

    # convert vocabulary overview to dataframe
    vocab_df = pd.DataFrame(vocab['vocab_overview'])

    # drop duplicate words
    vocab_df = vocab_df.drop_duplicates('word_string')

    # translating words into English
    def splitlist(mylist, chunk_size):
        """
        define a generator to split the list of words to be translated
        to chunks (to prevent 'Exception: Could not get translations'
        caused by long lists)
        """
        return [
            mylist[offs: offs + chunk_size] for
            offs in range(0, len(mylist), chunk_size)
        ]

    # split the list of words into chunks with a maximum length of 500
    word_list = splitlist(vocab_df['word_string'].tolist(), 500)

    # create empty dataframe for merging
    vocab_merge = pd.DataFrame()

    # translate each chunk and merge into the dataframe in a loop
    for t in word_list:
        # get translations from Duolingo's dictionary
        trnslt = lingo.get_translations(t, source=code, target='en')

        # convert translation dictionary to dataframe
        trnslt_df = pd.DataFrame(dict([
            (k, pd.Series(v, dtype='string')) for k, v in trnslt.items()
        ])).transpose()

        # combine translation values into a single column
        trnslt_df = pd.DataFrame(
            trnslt_df.apply(lambda x: x.str.cat(sep=', '), axis=1),
            columns=['translation']
        )

        # reset index and add keys to word_string column
        trnslt_df.index.names = ['word_string']
        trnslt_df.reset_index(level=0, inplace=True)

        # concatenate translated values from each loop
        # with the empty dataframe created
        vocab_merge = pd.concat([vocab_merge, trnslt_df])

    # merge translated values with vocabulary overview
    vocab_df = vocab_df.merge(vocab_merge, on=['word_string'])

    # drop unnecessary columns
    vocab_df = vocab_df.drop([
        'id', 'last_practiced_ms', 'lexeme_id', 'normalized_string',
        'related_lexemes', 'skill_url_title', 'strength'
    ], axis=1)

    # sort values by word_string, then skill
    vocab_df = vocab_df.sort_values(['word_string', 'skill'])

    # save vocabulary data as CSV
    vocab_df.to_csv('vocab_' + code + '.csv', index=None)

    # convert vocab into LaTeX glossary
    file = open('vocab_.tex', mode='w', encoding='utf-8')
    file.write(
        # double slash to escape the character
        # glossary title
        '\\renewcommand*{\\glossaryname}{\\centering\\Huge\\scshape'
        '\\MakeLowercase{' + name + ' -- English Dictionary}}\n'
    )
    # create glossary entries
    for index, row in vocab_df.iterrows():
        file.write(
            '\\newglossaryentry{' + str(row['word_string']) +
            '}{name={' + str(row['word_string']) + '},description={' +
            str(row['translation']) + '}}\n'
        )
    file.close()

    # compile LaTeX document into PDF
    system('xelatex vocab.tex')
    system('makeglossaries vocab')
    system('xelatex vocab.tex')

    # rename LaTeX glossary and PDF output to match the language
    rename('vocab_.tex', 'vocab_' + code + '.tex')
    rename('vocab.pdf', 'vocab_' + code + '.pdf')

    # rename log file to match language
    rename('vocab.log', 'vocab_' + code + '.log')

    # delete temporary files
    tempfiles = [
        'vocab.aux', 'vocab.glg', 'vocab.glo',
        'vocab.gls', 'vocab.ist', 'vocab.out'
    ]
    for tempfile in tempfiles:
        try:
            remove(tempfile)
        # exception if tempfile doesn't exist
        except OSError as e:
            print('Error: ' + str(e.filename) + ' - ' + str(e.strerror))
