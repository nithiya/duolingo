# Duolingo

Extracting learned [Duolingo](https://www.duolingo.com/) vocabulary with their English translations using the [Unofficial Duolingo API](https://github.com/KartikTalwar/Duolingo).

Duolingo has a [built-in feature to display learned words](https://www.duolingo.com/words). However, this is only available for a number of languages. It also does not display the translations within the learned words table. The [Python](https://www.python.org/) script, [`duolingo_data.py`](duolingo_data.py), resolves these issues by producing CSV and PDF files (via [LaTeX](https://www.latex-project.org/)) of the vocabulary with English translations, extracted from Duolingo's dictionary and [vocabulary overview](https://www.duolingo.com/vocabulary/overview).

## Installing dependencies

Running the script and building PDF files require installations of Python 3 and LaTeX.

### Cloning this repository

- *Option 1* - via HTTPS:

  ```sh
  git clone https://gitlab.com/nithiya/duolingo.git
  ```

- *Option 2* - via SSH:

  ```sh
  git clone git@gitlab.com:nithiya/duolingo.git
  ```

### Python

After installing Python 3, create and activate a virtual environment, and install all dependencies:

- *Option 1* - using [`venv`](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/):

  on Linux:

  ```sh
  python3 -m venv env
  source env/bin/activate
  python -m pip install -r requirements.txt
  ```

  on Windows:

  ```powershell
  py -m venv env
  .\env\Scripts\activate
  py -m pip install -r requirements.txt
  ```

- *Option 2* - using [Anaconda](https://www.anaconda.com/) (I recommend the lightweight [Miniconda](https://docs.conda.io/en/latest/miniconda.html)):

  ```sh
  conda create --name duolingo python=3 pandas requests
  conda activate duolingo
  python -m pip install duolingo-api
  ```

To view the full list of dependencies, see [`requirements.txt`](requirements.txt).

### LaTeX

All required LaTeX packages are available on [CTAN](https://www.ctan.org/). It is recommended to use a TeX distribution, such as [TeX Live](https://tug.org/texlive/), to ensure all requirements are satisfied.

PDF files are built using [`vocab.tex`](vocab.tex) via [XeLaTeX](https://www.ctan.org/pkg/xetex) and [glossaries](https://www.ctan.org/pkg/glossaries):

```sh
xelatex vocab.tex
makeglossaries vocab
xelatex vocab.tex
```

## Running the script

Define the languages you are learning in [`languages.conf`](languages.conf). Then, run the Python script:

```sh
python duolingo_data.py
```

Enter your Duolingo username and password when prompted in the terminal and press `enter` to continue.

After this script has finished running, CSV, JSON, TeX, and PDF files containing the vocabulary will be saved in the same directory, using the naming convention `vocab_XX`. `XX` refers to the language code as defined by Duolingo. The codes in the table below are the ones I am aware of (mostly corresponding to [ISO 639](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)). When tested with the dictionary URL (e.g., `https://d2.duolingo.com/api/1/dictionary/hints/en/de?token=hello` -- for translating 'hello' from English to German), the languages below produced an output.

**Note:** This script is not guaranteed to work for all languages, as it has only been tested on a small subset of languages. (**Last checked: May 2022.**)

Code | Language | Alternate code
--- | --- | ---
ar | Arabic
cs | Czech
cy | Welsh
da | Danish
de | German
dn | Dutch | nl-NL
el | Greek
eo | Esperanto
es | Spanish
fi | Finnish
fr | French
ga | Irish
gd | Scottish Gaelic
he | Hebrew
hi | Hindi
ht | Haitian Creole
hu | Hungarian
hv | High Valyrian
hw | Hawaiian
id | Indonesian
it | Italian
ja | Japanese
kl | Klingon | tlh
ko | Korean
la | Latin
nb | Norwegian (Bokmål) | no-BO
nv | Navajo
pl | Polish
pt | Portuguese
ro | Romanian
ru | Russian
sv | Swedish
sw | Swahili
tr | Turkish
uk | Ukrainian
vi | Vietnamese
yi | Yiddish
zs | Chinese | zh

## License

The contents of this repository are licensed under the terms of the [MIT License](https://opensource.org/licenses/MIT).
